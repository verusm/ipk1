/*                                                                            
*   Nazev: webinfo.h                                                    
*   Autor: Vera Mullerova, xmulle17@stud.fit.vutbr.cz                                                                 
*   Datum: 17.3.2011                                                          
*   Popis: Hlavickovy souboru pro webinfo.c.             
*/

#ifndef _WEBINFO_H_
#define _WEBINFO_H_

#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <ctype.h>
#include <string.h>
#include <string>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <netdb.h>
#include <regex.h>
#include <locale.h>

#include <iostream>

#define COUNT 4		/* na vstupu mohou byt pouze 4 parametry a pripadne 1 spatny (ten zjisti getopt) */
/* pokud je zadano vice jak 5 parametru, muze dojde k neopravnenemu pristupu do pameti */
#define CPARAM 5
#define BUFFER_LEN 100

using namespace std;

/** Struktura pro zachovani parametru. */
typedef struct{
  char param;		// parametr
  bool existed;		// byl zadan = true
}tFlag;

/** Deklarace pouzivanych funkci. */
int GetParams(int argc, char **argv, tFlag flag[]);
void ChangeUrl(string c, string ch, string *url);
int ControlURL(int *port, string *path, string *host, string url);
int WriteReadMsg(string path, string host, int port, string *messagefrom);
int ControlCodes(string *messagefrom, string *returnmsg);
int Statement(tFlag flag[], string *messagefrom);
int StatementAccordingParams(string *messagefrom, string header);
int ControlErrorStatus(int status, string msg, string path, string host, int post, string url, tFlag flag[], string returnmsg);

#endif