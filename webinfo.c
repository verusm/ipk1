/*                                                                            
*   Nazev: webinfo.c                                                    
*   Autor: Vera Mullerova, xmulle17@stud.fit.vutbr.cz                                                                 
*   Datum: 8.3.2011                                                          
*   Popis: Webovy klient pro zjisteni informaci o objektu.             
*/

// vlozeni knihovny, ve ktere jsou definovane vsechny pouzivane funkce a k nim potrebne knihovny
#include "webinfo.h"
int er300 = 0; // globalni promenna pro urceni poctu smerovani pri chybe 3xx

/** Hlavni program */  
int main (int argc, char **argv) {
  tFlag flag[COUNT];	// pole parametru
  // implicitni nastaveni false pro kazdy prvek pole
  for (int i=0; i<=COUNT; i++){	
    flag[i].existed = false;
  }
  // pomocne promenne
  int error;
  int optind;
  optind = GetParams(argc, argv, &flag[0]); // zpracovani parametru do pole flag[COUNT]
  if (optind == -1){
    return EXIT_FAILURE;
  }
  argc += 1;
  argc -= optind;
  argv += optind;
  // kontrola povinneho parametru URL
  if (argc < 2){
    fprintf(stderr, "Chybi povinny parametr URL!\n");
    return EXIT_FAILURE;
  }
  
  string url = *argv;	// do promenne url se zapise posledni parametr
  int port = 80;	// implicitni nastaveni portu
  string path = "/"; 	// implicitni nastaveni cesty
  string host;		// deklarace promenne pro hosta

  // nahrazeni pozadovanych znaku v URL - jeste lepsi by asi bylo je dat napr. do pole a prochazet pole
  string c;	// hledany znak
  string ch;	// nahrazujici znak 
  c = "%"; ch = "%25";
  ChangeUrl(c, ch, &url);
  c = " "; ch = "%20";
  ChangeUrl(c, ch, &url);
  c = ";"; ch = "%3B";
  ChangeUrl(c, ch, &url);
  c = "$"; ch = "%24";
  ChangeUrl(c, ch, &url);
  c = "\\"; ch = "%5C";
  ChangeUrl(c, ch, &url);

  int status = ControlURL(&port, &path, &host, url);
  if (status != 0){
    return EXIT_FAILURE;
  }

  // nastaveni zpravy pro server
  string msg = "HEAD " + path + " HTTP/1.1 \r\n" + "Host: " + host + " \r\n" + "Connection: close" + " \r\n\r\n";
  status = WriteReadMsg(path, host, port, &msg);	// zaslani a prijeti zpravy od serveru
  if (status != 0){
    return EXIT_FAILURE;
  }
  
  string returnmsg = "";	// string pro pripadne vypsani chyboveho kodu a textu
  status = ControlCodes(&msg, &returnmsg);	// kontrola chyboveho kodu vraceneho v hlavicce od serveru
  if (status == -1){
    return EXIT_FAILURE;
  }
  
  error = ControlErrorStatus(status, msg, path, host, port, url, &flag[0], returnmsg);	// zpracovani vypisu podle chyboveho kodu
  
  if (error == -1)
    return EXIT_FAILURE;
  return EXIT_SUCCESS;       
}

/** 
 * Funkce pro zpracovani vstupnich parametru. 
 * @param argc Pocet argumentu.
 * @param **argv Argumenty.
 * @param flag[] Pole pro zpracovani parametru, ktere je predavano hodnotou.
 * @return Vraci optind z funkce getopt.
 */
int GetParams(int argc, char **argv, tFlag flag[]){
  if (argc < 2){
    fprintf(stderr, "Chybi povinny parametr URL!\n");
    return -1;
  }
  
  int ch, c = 0;
  // cyklus se provadi dokud jsou na vstupu nejake parametry zacinajici pomlckou
  while((ch = getopt(argc, argv, "lsmt")) != -1){
    switch(ch){
      case 'l':
	  flag[c].param = 'l';
	  flag[c].existed = true;
	  c++;
	break;
      case 's':
	  flag[c].param = 's';
	  flag[c].existed = true;
	  c++;
	break;
      case 'm':
	  flag[c].param = 'm';
	  flag[c].existed = true;
	  c++;
	break;
      case 't':
	  flag[c].param = 't';
	  flag[c].existed = true;
	  c++;
	break;
      default:
	  return -1;
	break;
    }
  }
  return optind; 
}

/**
 * Funkce pro zamenu nepodporovanych znaku (napr. mezera) v URL adrese.
 * @param c Znak (rezetec), ktery ma byt nahrazen.
 * @param ch Rezetec, kterym ma byt pozadovany znak nahrazen.
 * @param *url URL adresa, ve ktere se nahrazuje.
 */
void ChangeUrl(string c, string ch, string *url){
  string nurl = *url;  
  size_t found = 0;
  while (found != string::npos){
    found = nurl.find(c, found+1);
    if (found != string::npos)
      nurl.replace(found,c.length(),ch);    
  }
  *url = nurl;
}

/** 
 * Funkce pro kontrolu a zpracovani URL.
 * @param *port Cislo portu.
 * @param *path Cesta.
 * @param *host Jmeno hosta.
 * @param url Url adresa, ze ktere jsou ziskany hodnoty port, path a host.
 * @return Vraci pripadnou chybu. 
 */
int ControlURL(int *port, string *path, string *host, string url){
  regmatch_t pmatch[CPARAM];
  string pattern = "^http://([^:/?]+)(:([0-9]+){1})?([/?].*)?$";	// regularni vyraz pro kontrolu URL protokolu HTTP
  
  regex_t re;
  int status;
  
  if(regcomp( &re, pattern.c_str(), REG_EXTENDED)!= 0) {	// regcomp => prevod do tvaru pro regexec
    fprintf(stderr, "Error pri kompilaci regexp!\n");
    return -1;
  }
    
  status = regexec(&re, url.c_str(), 5, pmatch, 0);

  // rozdeleni (rozparserovani) URL na jednotlive potrebne casti
  if(status == 0){
    *host = url.substr(pmatch[1].rm_so, pmatch[1].rm_eo-pmatch[1].rm_so);
    if (pmatch[3].rm_so > 0){
      *port = atoi((url.substr(pmatch[3].rm_so, pmatch[3].rm_eo-pmatch[3].rm_so)).c_str());
    }
    if (pmatch[4].rm_so > 0){
      *path = url.substr(pmatch[4].rm_so, pmatch[4].rm_eo-pmatch[4].rm_so);
    }
  }
  else {
    fprintf(stderr, "Špatná URL adresa!\n");
    return status;
  }
  regfree( &re);  
  return status;
}

/** 
 * Funkce pro zaslani a prijimani zpravy.
 * @param path Cesta.
 * @param host Jmeno hosta.
 * @param port Cislo portu.
 * @param *messagefrom Zprava, kterou chceme odesilat serveru.
 * @return Vraci pripadnou chybu.
 */
int WriteReadMsg(string path, string host, int port, string *messagefrom){
  int s, n;	// pomocne promenne
  string msg = *messagefrom;
  struct sockaddr_in sin;	 // struktura obsahujici port a adresu
  struct hostent *hptr;		 // struktura obsahujici "entity" hosta
  
  if ((s = socket(PF_INET, SOCK_STREAM, 0 )) < 0) {	// vytvoreni soketu
    fprintf(stderr, "Error na prikazu socket!\n");
    return -1;
  }
  
  sin.sin_family = PF_INET;	// nastaveni rodiny protokolu
  sin.sin_port = htons(port);	// nastaveni cisla portu
  
  if ((hptr =  gethostbyname(host.c_str())) == NULL){
    fprintf(stderr, "Gethostname error: %s \n", host.c_str());
    return -1;
   }
   
  memcpy( &sin.sin_addr, hptr->h_addr, hptr->h_length);
  
  if (connect (s, (struct sockaddr *)&sin, sizeof(sin) ) < 0){		// navazani spojeni
    fprintf(stderr, "Error na prikazu connect!\n");
    return -1;
  }
  
  if (write (s, msg.c_str(), strlen(msg.c_str()) +1) < 0) { 	// odesilani zpravy serveru
    fprintf(stderr, "Error na prikazu write!\n");
    return -1;
  }
  
  int stat = 1;
  char message[2];
  msg.clear();	// vymazani retezce msg
  
  // cyklus pro cteni odpovedi od serveru
  while(stat){
    if (( n = read(s, message, 1)) < 0) {	// cteni odpovedi
      fprintf(stderr, "Error na prikazu read!\n");
      return -1;
    }    
    /* overeni konce prijate zpravy - kontrola, zde neni hlavicka ukoncena pomoci \r\n\r\n */
    if(message[0] == '\r'){
      msg = msg + message[0];
      if (( n = read(s, message, 1)) < 0) {	// cteni odpovedi
	fprintf(stderr, "Error na prikazu read!\n");
	return -1;
      }      
      if (message[0] == '\n'){
	msg = msg + message[0];	
	if (( n = read(s, message, 1)) < 0) {	// cteni odpovedi
	  fprintf(stderr, "Error na prikazu read!\n");
	  return -1;
	}
	if (message[0] == '\r'){
	  msg = msg + message[0];
	  if (( n = read(s, message, 1)) < 0) {	// cteni odpovedi
	    fprintf(stderr, "Error na prikazu read!\n");
	    return -1;
	  }
	  if (message[0] == '\n'){
	    msg = msg + message[0];
	    stat = 0;
	  }
	  else
	    msg = msg + message[0];
	}
	else
	  msg = msg + message[0];
      }
      else
	msg = msg + message[0];
    }
    // kontrola, zda hlavicka neni ukoncena pomoci \n\n
    else if (message[0] == '\n'){
      msg = msg + message[0];
      if (( n = read(s, message, 1)) < 0) {	// cteni odpovedi
	fprintf(stderr, "Error na prikazu read!\n");
	return -1;
      }
      if (message[0] == '\n'){
	msg = msg + message[0];
	stat = 0;
      }
      else
	msg = msg + message[0];
    }
    else
      msg = msg + message[0];
  }
  
  if (close(s) < 0) { 	// ukonceni spojeni
    fprintf(stderr, "Error na prikazu close!");
    return -1;
  }
  
  *messagefrom = msg;
  return 0;
}

/** 
 * Funkce pro kontrolu a pripadne zpracovani chyby z odpovedi od serveru.
 * @param *messagefrom Zprava, ktera je zaslana serveru.
 * @param *returnmsg String pro pripadny chybovy kod a text chyby vraci serverem.
 * @return Vraci kod chyby od serveru nebo pripadnou chybu v programu.
 */
int ControlCodes(string *messagefrom, string *returnmsg){
  string msg = *messagefrom;
  string rmsg = *returnmsg;
  int status;
  int fault;
  
  regmatch_t pmatch[CPARAM];
  string pattern = "^HTTP/1.1 ([0-9]+) ([^\r\n]+)\r\n";		// regularni vyraz pro kontrolu odpovedi (hlavicky)
  
  regex_t re; 
  if(regcomp( &re, pattern.c_str(), REG_EXTENDED)!= 0) {	// regcomp => prevod do tvaru pro regexec
    fprintf(stderr, "Error pri kompilaci regexp!\n");
    return -1;
  }
    
  status = regexec(&re, msg.c_str(), 5, pmatch, 0);
  // vraceni kodu chyby od serveru
  if(status == 0){
    rmsg = msg.substr(pmatch[1].rm_so, pmatch[2].rm_eo-pmatch[1].rm_so);
    *returnmsg = rmsg;
    return fault = atoi((msg.substr(pmatch[1].rm_so, pmatch[1].rm_eo-pmatch[1].rm_so)).substr(0,1).c_str());
  }
  else {
    fprintf(stderr, "Hlavička neobsahuje řádek s návratovým kódem!\n");
    return -1;
  }
  regfree( &re);  

  *returnmsg = rmsg;
  *messagefrom = msg;
  return status;
}

/** 
 * Funkce pro vypis hlavicky nebo pouze pozadujicich radku.
 * @param flag[] Pole parametru.
 * @param *messagefrom 
 * @return Vraci pripadnou chybu.
 */
int Statement(tFlag flag[], string *messagefrom){
  string msg = *messagefrom;
  char param;
  string header;	// promenna pro nazev hlavicky radku, ktery pozadujeme
  int error;
  
  if (!flag[0].existed){	// pokud nejsou zadne parametry, vypise se cela odpoved
    cout << msg << endl;
  }
  else {	// jinak se vypisuji pouze pozadovane radky
    for (int i=0; flag[i].existed; i++){
      param = flag[i].param;
      switch(param){
	case 'l':
	    header = "Content-Length";
	    error = StatementAccordingParams(&msg, header);
	  break;
	case 's':
	    header = "Server";
	    error = StatementAccordingParams(&msg, header);
	  break;
	case 'm':
	    header = "Last-Modified";
	    error = StatementAccordingParams(&msg, header);
	  break;
	case 't':
	    header = "Content-Type";
	    error = StatementAccordingParams(&msg, header);
	  break;
	default:
	    return -1;
	  break;
      }
      if (error == -1)
	return -1;
    }
  }
  
  *messagefrom = msg;
  return 0;
}

/**
 * Funkce pro vypsani pozadovaneho radku ze zpravy od serveru.
 * @param *messagefrom Zprava od serveru.
 * @param header Hlavicka radku, ktery se ma vypsat.
 * @return Vraci pripadnou chybu.
 */
int StatementAccordingParams(string *messagefrom, string header){
  string msg = *messagefrom;
  int status;
  
  regmatch_t pmatch[CPARAM];
  string pattern = "\n" + header + "(:[^\r\n]+)\r\n";		// regularni vyraz pro jednotlive radky hlavicky (podle parametru)
  
  regex_t re; 
  if(regcomp( &re, pattern.c_str(), REG_EXTENDED)!= 0) {	// regcomp => prevod do tvaru pro regexec
    fprintf(stderr, "Error while compiling regexp !\n");
    return -1;
  }
    
  status = regexec(&re, msg.c_str(), 5, pmatch, 0);
  if(status == 0){
    if (header == "Location"){	// pouziti pro zjisteni URL adresy pri chybe 3xx
      msg = msg.substr(pmatch[1].rm_so, pmatch[1].rm_eo-pmatch[1].rm_so);
      msg = msg.substr(2);
    }
    else {	// vypsani pozadovaneho radku
      cout << header;
      cout << msg.substr(pmatch[1].rm_so, pmatch[1].rm_eo-pmatch[1].rm_so) << endl;
    }
  }
  else {
    if (header == "Location"){
      msg.clear();
    }
    else	// pokud radek neni ve zprave obsazen vypise se N/A
      printf("%s: N/A\n", header.c_str());
  }
  regfree( &re);
  *messagefrom = msg;
  return 0;
}

/**
 * Funkce pro zpracovani chyboveho kodu vraceneho od severu.
 * @param status Prvni cislice chyboveho kodu.
 * @param msg Zprava od serveru.
 * @param path Cesta.
 * @param host Host.
 * @param port Cislo portu.
 * @param url URL adresa.
 * @param flag[] Pole se zpracovanymi parametry.
 * @param returnmsg Retezec pro pripadny vypis chyboveho hlaseni od serveru.
 * @return Vraci pripadnou chybu.
 */
int ControlErrorStatus(int status, string msg, string path, string host, int port, string url, tFlag flag[], string returnmsg){ 
  int error = 0;	// pomocna promenna pro navratovou hodnotu chyby
  switch(status){
    case 2:	// 200 OK
	error = Statement(&flag[0], &msg);
      break;
    case 3:	// 3xx
	fprintf(stderr, "Chyba: %s\n", returnmsg.c_str());	// vypsani chyboveho hlaseni od severu
	if (er300 < 5){		// podpora 5 presmerovani
	  er300++;
	  error = 3;
	  StatementAccordingParams(&msg, "Location");	// ziskani nove URL adresy ze radku Location
	  url = msg;
	  port = 80; path = "/"; host = "";	// nastaveni implicitnich hodnot
	  status = ControlURL(&port, &path, &host, url);
	  if (status != 0){
	    return EXIT_FAILURE;
	  }
	  // nastaveni zpravy pro server
	  msg = "HEAD " + path + " HTTP/1.1 \r\n" + "Host: " + host + " \r\n" + "Connection: close" + " \r\n\r\n";
	  status = WriteReadMsg(path, host, port, &msg);
	  if (status != 0){
	    return -1;
	  }
	  returnmsg = "";
	  status = ControlCodes(&msg, &returnmsg);
	  if (status == -1){
	    return -1;
	  }
	  error = ControlErrorStatus(status, msg, path, host, port, url, &flag[0], returnmsg);  
	    
	  if (error != 3){	// ukonceni rekurze, pokud uz neni treba presmerovavat
	    break;
	  } 
	}
	fprintf(stderr, "Více přesměrování není akceptováno!\n");	// vypsani chyby pri opakovane nezdarenem presmerovani
      break;
    case 4:	// kody 4xx a 5xx
    case 5:
        fprintf(stderr, "Chyba: %s\n", returnmsg.c_str());
	error = Statement(&flag[0], &msg);
      break;
    default:
	return -1;
      break;
  }
  return error;
}