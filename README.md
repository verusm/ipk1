# README #

This program is a client using sockets API which gives you an information about given object (URL).

Compile by:

```
#!c

make
```

Usage:

```
#!c

webinfo http://www.google.com
```