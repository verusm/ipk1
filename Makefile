#
# Projekt: webinfo
# Autor:   Vera Mullerova, xmulle17@stud.fit.vutbr.cz
# Datum:   25.2.2011
#

CC=g++
CFLAGS=-Wall -pedantic -g
NAME=webinfo

$(NAME): webinfo.c
	$(CC) $(CFLAGS) webinfo.c -o $(NAME)

.PHONY: clean

clean:
	rm -f *.o *~
